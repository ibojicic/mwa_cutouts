from setuptools import setup

setup(
    name='cut_mwa',
    version='0.1',
    py_modules=['cut_mwa'],
    install_requires=[
        'Click',
    ],
    entry_points={
        'console_scripts': [
            'cut_mwa=cut_mwa:cli',
            'cut_mwa_rms=cut_mwa_rms:cli'

        ]},
)
