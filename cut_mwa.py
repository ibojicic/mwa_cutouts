import datetime
import sys
import click
import ibcommon.parse as ib_parse
from ibcommon import dicts
import ibplotting.plotlibs as ibplot

from pprint import pprint
from MWACutoutsClass import MWACutouts
import os
import MWAdbModels

types = ['mos', 'bkg', 'rms']
make_images = ['out_mos_image', 'out_bkg_image', 'out_rms_image', 'out_rebkg_image', 'out_corr_image']
mwa_bands = ['072-080', '080-088', '088-095', '095-103', '103-111',
              '111-118', '118-126', '126-134', '139-147', '147-154',
              '154-162', '162-170', '170-177', '177-185', '185-193',
              '193-200', '200-208', '208-216', '216-223', '223-231',
              '072-103', '103-134', '139-170', '170-231']
mwa_weeks = ['v', '2', '4']


@click.command()
@click.argument('catalogue', nargs=1, type=click.Choice(['pne', 'snrs', 'misc']))
@click.argument('results_path', nargs=1)
@click.option('--db_flag', '-f', default='all')
@click.option('--week', '-w', default='all')
@click.option('--obj_id', '-i', default='all')
@click.option('--band', '-b', default='all')
@click.option('--source_path', '-s', default='/Volumes/MWA/GP_MWA/')
@click.option('--rewrite', '-r', is_flag=True, default=False)
@click.option('--record_to_db', '-d', is_flag=True, default=False)
@click.option('--make_pngs', '-m', is_flag=True, default=False)

def cli(catalogue, week, results_path, db_flag, obj_id, band, rewrite, source_path, record_to_db, make_pngs):

    table_in, table_out = set_table(MWAdbModels, catalogue)
    input_objects = table_in.select(table_in.name, table_in.draj2000, table_in.ddecj2000, table_in.majdiam)

    if db_flag != 'all':
        input_objects = input_objects.where(table_in.flag == db_flag)

    if obj_id != 'all':
        input_objects = input_objects.where(table_in.name == obj_id)

    source_path = ib_parse.corrpath(source_path)
    results_path = ib_parse.corrpath(results_path)

    if make_pngs:
        pngs_path = "{}pngs/".format(results_path)
        if not os.path.exists(pngs_path):
            os.makedirs(pngs_path)


    for curr_week in mwa_weeks:
        if week != 'all' and week != curr_week:
            continue

        for curr_band in mwa_bands:
            if band != 'all' and band != curr_band:
                continue

            for curr_object in input_objects:

                object_path = "{}{}/".format(results_path,curr_object.name)

                mwacutout = MWACutouts(curr_object.name,
                                       source_path,
                                       object_path,
                                       curr_object.draj2000,
                                       curr_object.ddecj2000,
                                       curr_object.majdiam)

                mwacutout.rewrite = rewrite


                pprint("Working on id={} band={} week={}".format(curr_object.name,curr_band,curr_week))

                mwacutout.set_week(curr_week).set_band(curr_band)
                mwacutout.cut_image()
                if curr_week != 'v':
                    mwacutout.update_beam('mos').update_units('mos')
                    mwacutout.rebin_bkg().rebin_rms()
                    mwacutout.subtract_bkg()

                full_pars = dicts.merge_dicts([
                    mwacutout.inp('all'),
                    mwacutout.image_files,
                    mwacutout.beam_pars,
                    {'band':curr_band, 'week':curr_week,
                     'imsize':mwacutout.imsize, 'catalogue':catalogue,
                     'date': datetime.datetime.now()}
                        ])

                if make_pngs:
                    make_png(curr_week, mwacutout,pngs_path)
                if record_to_db and check_rewrite(rewrite,curr_object.name,curr_band,curr_week,table_out):
                    table_out.create(**full_pars)

def check_rewrite(rewrite, name, band, week, table_out):

    res = table_out.select().where(
        (table_out.name == name) &
        (table_out.band == band) &
        (table_out.week == week)
    )
    if res.count() == 0:
        # no results => continue
        return True
    elif res.count() > 1:
        print "Multiple results?"
        # multiple results (error) => do not continue
        return False
    if not rewrite:
        # 1 results and no rewrite => do not continue
        return False
    table_out.delete().where(table_out.idmwacutouts == res.get().idmwacutouts).execute()
    # old results deleted => continue
    return True


def set_table(model, catalogue):
    tablein = {'pne': model.PNcandidates, 'snrs': model.Galacticsnrs, 'misc': model.MiscObjects}
    tableout = {'pne': model.MwaCutoutsModel, 'snrs': model.SNRCutoutsModel, 'misc': model.MiscCutoutsModel}

    return tablein[catalogue], tableout[catalogue]


def make_png(curr_week,mwacutout,pngs_path):
    if curr_week == 'v':
        obs_type = "vo"
        input_file_name = mwacutout.imfile('out', 'vo')
        input_file_path = mwacutout.full_file_path(mwacutout.inp('outpath'),
                                                    obs_type, input_file_name)
    else:
        obs_type = "mos"
        input_file_name = mwacutout.imfile('out', 'mos')
        input_file_path = mwacutout.full_file_path(mwacutout.inp('outpath'),
                                                    obs_type, input_file_name)

    if input_file_name and input_file_path:
        output_file_path = "{}{}.png".format(pngs_path,input_file_name)
        fit_crosses = [[mwacutout.inp('draj2000'), mwacutout.inp('ddecj2000'),mwacutout.inp('name')]]

        ibplot.photFigure(input_file_path,output_file_path,
                          mwacutout.inp('draj2000'), mwacutout.inp('ddecj2000'),
                          imsize=mwacutout.imsize/(6*3600.),
                          crosses=fit_crosses,
                          PNDiam = mwacutout.inp('diam'),
                          percmin=10,
                          percmax=98,
                          )

                          # mwacutout.inp('diam')
                          #   self.obj.draj2000, self.obj.ddecj2000, self.obs.pars['annulus_in'] / 3600.,
                          # self.obs.pars['annulus_out'] / 3600., self.obs.pars['radius'] / 3600.,
                          # self.obj.majdiam / 3600., cont_levels, -1,
                          # rLevels=rms_levels, crosses=fit_crosses, imsize=2. * self.obs.pars['annulus_out'] / 3600.)




