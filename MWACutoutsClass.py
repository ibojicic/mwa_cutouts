import math
import os
import shutil
import ibastro.fitslibs as ib_fitslibs
import ibcommon.parse as ib_parse
import ibexternal.gleam_client as gc


# noinspection SpellCheckingInspection,SpellCheckingInspection
class MWACutouts:
    # minimum size of the cutout (arcsec)
    _minsize = 3600
    _mwa_imtypes = ['mos', 'bkg', 'rms']


    def __init__(self, object_name, mosaics_path, out_path, draj2000, ddecj2000, diam):
        self._inp = None
        self._week = None
        self._band = None
        self._image_files = None
        self._beam_pars = None
        self._imsize = None
        self._rewrite = False

        # set input parameters
        self.set_inp('name', object_name)
        self.set_inp('outpath', out_path).check_outpath()
        self.set_inp('rootpath', mosaics_path).check_rootpath()
        self.set_inp('draj2000', draj2000)
        self.set_inp('ddecj2000', ddecj2000)
        self.set_inp('diam', diam)
        self.set_coutout_size()

    def set_inp(self, key, value):
        if self._inp is None:
            self._inp = {}
        self._inp[key] = value
        return self

    # get input parameter
    def inp(self, key="all"):
        if key == 'all':
            return self._inp
        elif key in self._inp:
            return self._inp[key]
        return False

    @property
    def imsize(self):
        return self._imsize

    @imsize.setter
    def imsize(self, value):
        self._imsize = value

    @property
    def beam_pars(self):
        return self._beam_pars

    @property
    def rewrite(self):
        return self._rewrite

    @rewrite.setter
    def rewrite(self, value):
        if isinstance(value, bool):
            self._rewrite = value

    def check_outpath(self):
        value = ib_parse.corrpath(self.inp('outpath'))
        if not os.path.exists(value):
            os.makedirs(value)

    def check_rootpath(self):
        path = ib_parse.corrpath(self.inp('rootpath'))
        if not os.path.exists(path):
            raise IOError("Folder {} doesn't exist.".format(path))

    @property
    def mospath(self):
        if not self.inp('rootpath') or self.week is None:
            raise BaseException("Variables root path and/or week have not been defined")
        return '{}Week{}/'.format(self.inp('rootpath'), self.week)

    @property
    def week(self):
        return self._week

    def set_week(self, value):
        self._week = value
        return self

    def set_band(self, value):
        self._band = value
        return self

    def set_coutout_size(self):
        if not isinstance(self.inp('diam'), (float, int)):
            raise BaseException("Variable diam is not numeric.")

        if 3. * self.inp('diam') < self._minsize:
            self.imsize = self._minsize
        else:
            self.imsize = 3. * self.inp('diam')
        return self

    def cut_image(self):
        if self.week == 'v':
            self.get_vocutout()
        else:
            self.get_mosaics()
        return self

    @property
    def image_files(self):
        return self._image_files

    def imfile(self, inout, obs_type):
        key = self.imkey(inout, obs_type)
        if self._image_files is None or key not in self._image_files:
            return False
        return self._image_files[key]

    def get_mosaics(self):
        for obs_type in self._mwa_imtypes:
            self.imsize = self.imsize if obs_type == 'mos' else self.imsize * 2.

            input_file_name = self.input_file_name(obs_type)
            input_file_path = self.full_file_path(self.mospath, obs_type, input_file_name)

            if not os.path.exists(input_file_path):
                raise BaseException("Input file {} doesn't exist.".format(input_file_path))

            self.set_image_file(input_file_name, "in", obs_type)

            output_file_name = self.output_file_name(obs_type)
            output_file_path = self.full_file_path(self.inp('outpath'), obs_type, output_file_name, True)

            if self.create_image(output_file_path,
                                 self.cut_mosaic,
                                 input_file_path,
                                 output_file_path):
                self.set_image_file(output_file_name, "out", obs_type)

    def cut_mosaic(self, in_image, out_image):
        if not ib_fitslibs.FITScutout(in_image,
                                      out_image,
                                      self.inp('draj2000'), self.inp('ddecj2000'),
                                      self.imsize):
            return False

        X, Y = ib_fitslibs.coord2pix(out_image, self.inp('draj2000'), self.inp('ddecj2000'))
        if not ib_fitslibs.checkValidVal(out_image, X, Y, -1.E15, 1.E15):
            os.remove(out_image)
            return False
        return True

    def get_vocutout(self):

        output_file_name = self.output_file_name("vo")
        output_file_path = self.full_file_path(self.inp('outpath'), "vo", output_file_name, True)
        if self.create_image(output_file_path,
                             self.cut_vocutout,
                             output_file_path):
            self.set_image_file(output_file_name, "out", "vo")
            self.get_beam_pars(output_file_path)

    def cut_vocutout(self, output_file_path):
        imsize = math.ceil(self.imsize * 2 / 3600)
        imsize = imsize if imsize < 5. else 5.
        self.imsize = imsize * 3600

        gc.vo_get(self.inp('draj2000'),
                  self.inp('ddecj2000'),
                  imsize,
                  freq=self._band,
                  proj_opt='ZEA',
                  download_dir="{}vo".format(self.inp('outpath')),
                  vo_host='gleam-vo.icrar.org')

        cutout_name = gc.create_filename(self.inp('draj2000'), self.inp('ddecj2000'), imsize, self._band)
        downloaded = self.full_file_path(self.inp('outpath'), "vo", cutout_name, True)
        if os.path.exists(downloaded):
            shutil.move(downloaded, output_file_path)

        return True

    def input_file_name(self, obs_type):
        wide_bands = ['072-103', '103-134', '139-170', '170-231']
        wide_band_pref = ""
        if self._band in wide_bands:
            wide_band_pref = 'wideband_'
        return '{}mosaic_Week{}_{}MHz_{}.fits'.format(wide_band_pref, self._week, self._band, obs_type)

    def output_file_name(self, obs_type):
        if obs_type != "vo":
            return "{}_{}".format(self.inp('name'), self.input_file_name(obs_type))
        elif obs_type == "vo":
            return "{}_{}MHz_vo.fits".format(self.inp('name'), self._band)
        return False

    @staticmethod
    def full_file_path(path, obs_type, file_name, make=False):

        filepath = "{}{}".format(path, obs_type)
        if not os.path.exists(filepath):
            if not make:
                return False
            os.makedirs(filepath)
        return "{}/{}".format(filepath, file_name)

    def rebin_bkg(self):
        if not (self.imfile('out', 'bkg') and self.imfile('out', 'mos')):
            return self

        rebkg_file_name = self.output_file_name('rebkg')
        rebkg_file_path = self.full_file_path(self.inp('outpath'), 'rebkg', rebkg_file_name, True)

        bkg_file_path = self.full_file_path(self.inp('outpath'), 'bkg', self.imfile('out', 'bkg'))
        mos_file_path = self.full_file_path(self.inp('outpath'), 'mos', self.imfile('out', 'mos'))

        if self.create_image(rebkg_file_path,
                             ib_fitslibs.reprojectOnTemplate,
                             bkg_file_path,
                             rebkg_file_path,
                             mos_file_path
                             ):
            self.set_image_file(rebkg_file_name, "out", "rebkg")

        return self

    def rebin_rms(self):
        if not (self.imfile('out', 'rms') and self.imfile('out', 'mos')):
            return self

        rerms_file_name = self.output_file_name('rerms')
        rerms_file_path = self.full_file_path(self.inp('outpath'), 'rerms', rerms_file_name, True)

        rms_file_path = self.full_file_path(self.inp('outpath'), 'rms', self.imfile('out', 'rms'))
        mos_file_path = self.full_file_path(self.inp('outpath'), 'mos', self.imfile('out', 'mos'))

        if self.create_image(rerms_file_path,
                             ib_fitslibs.reprojectOnTemplate,
                             rms_file_path,
                             rerms_file_path,
                             mos_file_path
                             ):
            self.set_image_file(rerms_file_name, "out", "rerms")

        return self

    def subtract_bkg(self):
        if not (self.imfile('out', 'rebkg') and self.imfile('out', 'mos')):
            return self

        rebkg_file_path = self.full_file_path(self.inp('outpath'), 'rebkg', self.output_file_name('rebkg'), True)
        mos_file_path = self.full_file_path(self.inp('outpath'), 'mos', self.imfile('out', 'mos'))

        corr_file_name = self.output_file_name('corr')
        corr_file_path = self.full_file_path(self.inp('outpath'), 'corr', corr_file_name, True)

        if self.create_image(corr_file_path,
                             ib_fitslibs.doSubtraction,
                             mos_file_path,
                             rebkg_file_path,
                             corr_file_path, 1):
            self.set_image_file(corr_file_name, "out", "corr")

        return self

    def create_image(self, imagefile, inp_function, *pars):
        if os.path.exists(imagefile):
            if not self.rewrite:
                return False
            os.remove(imagefile)

        okrun = inp_function(*pars)

        if not os.path.exists(imagefile):
            return False
        elif not okrun:
            os.remove(imagefile)

            return False

        return True

    def set_image_file(self, imagefile, inout, obs_type):
        image_key = self.imkey(inout, obs_type)
        if self._image_files is None:
            self._image_files = {}
        self._image_files[image_key] = imagefile
        return self

    def update_beam(self, obs_type):
        # if file for beam update is not defined skip
        if not self.imfile("out", obs_type):
            return self

        input_file_name = self.input_file_name("psf")
        input_file_path = self.full_file_path(self.mospath, "psf", input_file_name)

        output_file_path = self.full_file_path(self.inp('outpath'), obs_type, self.imfile("out", obs_type))

        if not input_file_path or not os.path.exists(input_file_path):
            return self

        X, Y = ib_fitslibs.coord2pix(input_file_path, self.inp('draj2000'), self.inp('ddecj2000'))
        bv = ib_fitslibs.valueAtCoord(input_file_path, X, Y)
        if len(bv) == 4:
            try:
                self._beam_pars = {'BMAJ': bv[0], 'BMIN': bv[1], 'BPA': bv[2], 'BBLUR': bv[3]}
                ib_fitslibs.updateHeader(output_file_path, self._beam_pars)
            except Exception:
                self._beam_pars = {}

                return self

        return self

    def update_units(self,obs_type):
        # if file for beam update is not defined skip
        if not self.imfile("out", obs_type):
            return self
        output_file_path = self.full_file_path(self.inp('outpath'), obs_type, self.imfile("out", obs_type))
        ib_fitslibs.updateHeader(output_file_path, {'BUNIT':'JY/BEAM'})
        return self

    def get_beam_pars(self, inimage):
        self._beam_pars = ib_fitslibs.getHeaderItems(inimage, ['BMAJ', 'BMIN', 'BPA', 'BUNIT'])

    @staticmethod
    def imkey(inout, obs_type):
        return "{}_{}_image".format(inout, obs_type)
