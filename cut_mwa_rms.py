import datetime
import sys
import click
import ibcommon.parse as ib_parse
from ibcommon import dicts
import ibplotting.plotlibs as ibplot

from pprint import pprint
from MWACutoutsClass import MWACutouts
import os
import MWAdbModels
import ibastro.fitslibs as ib_fitslibs

types = ['mos', 'bkg', 'rms']
make_images = ['out_mos_image', 'out_bkg_image', 'out_rms_image', 'out_rebkg_image', 'out_corr_image']
mwa_bands = ['072-080', '080-088', '088-095', '095-103', '103-111',
              '111-118', '118-126', '126-134', '139-147', '147-154',
              '154-162', '162-170', '170-177', '177-185', '185-193',
              '193-200', '200-208', '208-216', '216-223', '223-231',
              '072-103', '103-134', '139-170', '170-231']
mwa_weeks = ['v', '2', '4']


@click.command()
@click.argument('catalogue', nargs=1, type=click.Choice(['pne', 'snrs']))
@click.argument('results_path', nargs=1)
@click.option('--db_flag', '-f', default='all')
@click.option('--week', '-w', default='all')
@click.option('--obj_id', '-i', default='all')
@click.option('--band', '-b', default='all')
@click.option('--source_path', '-s', default='/Volumes/MWA/GP_MWA/')
@click.option('--rewrite', '-r', is_flag=True, default=False)
@click.option('--record_to_db', '-d', is_flag=True, default=False)
@click.option('--make_pngs', '-m', is_flag=True, default=False)

def cli(catalogue, week, results_path, db_flag, obj_id, band, rewrite, source_path, record_to_db, make_pngs):

    table = set_table(MWAdbModels, catalogue)
    input_objects = table.select(table.name, table.draj2000, table.ddecj2000, table.majdiam)

    if db_flag != 'all':
        input_objects = input_objects.where(table.flag == db_flag)

    if obj_id != 'all':
        input_objects = input_objects.where(table.name == obj_id)

    source_path = ib_parse.corrpath(source_path)
    results_path = ib_parse.corrpath(results_path)

    if make_pngs:
        pngs_path = "{}pngs/".format(results_path)
        if not os.path.exists(pngs_path):
            os.makedirs(pngs_path)


    for curr_week in mwa_weeks:
        if week != 'all' and week != curr_week:
            continue

        for curr_band in mwa_bands:
            if band != 'all' and band != curr_band:
                continue

            for curr_object in input_objects:

                object_path = "{}{}/".format(results_path,curr_object.name)

                imgs = get_mwacoutots(curr_object.name,curr_week,curr_band)
                if imgs == None:
                    continue

                out_path = "{}rerms".format(object_path)
                if not os.path.exists(out_path):
                    os.makedirs(out_path)
                out_img = imgs['mos'].replace('mos.fits','rerms.fits')
                rermrs = "{}/{}".format(out_path,out_img)
                mos = "{}mos/{}".format(object_path,imgs['mos'])
                rms = "{}rms/{}".format(object_path,imgs['rms'])
                ib_fitslibs.reprojectOnTemplate(rms,rermrs,mos)
                if os.path.exists(rermrs):
                    update_rerms(curr_object.name,curr_week,curr_band,out_img)


def update_rerms(name, week, band, newimage):
    tbl = MWAdbModels.MwaCutoutsModel

    query = tbl.update(out_rerms_image=newimage).where(
        (tbl.name == name) &
        (tbl.band == band) &
        (tbl.week == week)
    )
    query.execute()

def get_mwacoutots(name,week,band):
    tbl = MWAdbModels.MwaCutoutsModel
    res = tbl.select().where(
        (tbl.name == name) &
        (tbl.band == band) &
        (tbl.week == week) &
        (tbl.out_mos_image.is_null(False)) &
        (tbl.out_rms_image.is_null(False))
    )
    print name,week,band
    if res.count() > 0:
        return {'mos':res.get().out_mos_image, 'rms':res.get().out_rms_image}
    return None



def check_rewrite(rewrite, name, band, week):
    tbl = MWAdbModels.MwaCutoutsModel

    res = tbl.select().where(
        (tbl.name == name) &
        (tbl.band == band) &
        (tbl.week == week)
    )
    if res.count() == 0:
        # no results => continue
        return True
    elif res.count() > 1:
        print "Multiple results?"
        # multiple results (error) => do not continue
        return False
    if not rewrite:
        # 1 results and no rewrite => do not continue
        return False
    tbl.delete().where(tbl.idmwacutouts == res.get().idmwacutouts).execute()
    # old results deleted => continue
    return True


def set_table(model, catalogue):
    tablemap = {'pne': model.PNcandidates, 'snrs': model.Galacticsnrs}
    return tablemap[catalogue]


